public class LinkedList<E> implements List<E> {

    // параметризованный узел
    private static class Node<V> {
        V value;
        Node<V> next;

        Node(V value) {
            this.value = value;
        }
    }

    // ссылка на первый элемент
    private Node<E> first;
    // ссылка на последний элемент
    private Node<E> last;

    private int size;

    @Override
    public void add(E element) {
        // для нового элемента создаем узел
        Node<E> newNode = new Node<>(element);
        // если список пустой
        if (isEmpty()) {
            // первый узел = новый узел
            first = newNode;
        } else {
            // следующий после последнего - это новый узел
            last.next = newNode;
        }
        // теперь новый узел - последний
        last = newNode;

        size++;
    }

    public void addToBegin(E element) {
        // для нового элемента создаем узел
        Node<E> newNode = new Node<>(element);

        if (isEmpty()) {
            last = newNode;
        } else {
            // для нового узла, следующий после него - это первый узел списка
            newNode.next = first;
        }

        // теперь новый узел - первый
        first = newNode;
        size++;
    }

    public void removeAt(int index) {
        Node<E> previousRemove = first;

        if (index == 0) {
            // если удаляется первый элемент, просто сдвигаем ссылку на него
            first = first.next;
            size--;
            return;
        }

        // доходим до индекса элемента, который стоит перед удаляемым
        for (int i = 0; i < index - 1; i++) {
            // переходим по узлам
            previousRemove = previousRemove.next;
        }
        // если удаляем последний
        if (index == (size - 1)) {
            last = previousRemove;
        }
        // в current находится узел, который стоит перед удаляемым
        Node<E> forRemove = previousRemove.next;
        previousRemove.next = forRemove.next;

        size--;
    }

    public void remove(E element) {
        Node<E> current = first;

        // если удаляемый элемент находится в самом начале списка
        if (element == current.value) {
            first = first.next;

            size--;
            return;
        }
        // если удаляемый элемент находится в середине списка или в самом конце
        while (current.next != null) {
            if (current.next.value == element) {
                current.next = current.next.next;

                size--;
                return;
            }
            current = current.next;
        }
    }

    public void removeLast(E element) {
        Node<E> current = first;

        while (current.next != null) {
            // если текущий элемент не равен удаляемому элементу
            if (current.value != element) {
                // то переходим к следующему
                current = current.next;
            } else {
                while (current != null) {
                    current = current.next;
                    if (current.next.value != element) {
                    }
                    else {
                        // удаляем крайний элемент
                        Node<E> forRemove = current.next;
                        current.next = forRemove.next;
                        return;
                    }
                    size --;
                }
            }
        }
    }

    public void removeAll(E element) {
        Node<E> current = first;
        // если удаляемый элемент первый в списке
        if (element == current.value) {
            first = first.next;

            size--;
        }
        // если удаляемый элемент находится в середине списка или в самом конце
        while (current.next != null) {
            if (current.next.value == element) {
                while (current.next.value == element) {

                    current.next = current.next.next;
                }

                size--;
            }
            current = current.next;
        }
    }

    public void add(int index, E element) {
        Node<E> current = first;
        Node<E> newElement = new Node<>(element);

        // если индекс добавляемого элемента равен 0
        if (index == 0) {
            // следующий элемент после нового - предыдущий первый
            newElement.next = first;
            // добавляемый элемент - первый
            first = newElement;

            size++;
            return;
        }

        current = current.next;

        // если добавляемый элемент последний
        if (index == size - 1) {
            last.next = newElement;

            // теперь новый узел - последний
            last = newElement;

            size++;
            return;
        }

        // если добавляемый элемент в середине или в конце списка
        for (int i = 1; i < size; i++) {
            // доходим до элемента, который должен стоять перед добавляемым
            if (i == index - 1) {
                // следующий элемент после добавляемого - следующий элемент списка
                newElement.next = current.next;
                // следующий элемент - добавляемый
                current.next = newElement;

                size++;
                return;
            }
            current = current.next;
        }
    }

    public void reverse() {
        // если список пустой
        if (isEmpty()) {
            return;
        }

        Node<E> previous = null;
        Node<E> current = first;

        while (current != null) {
            Node<E> next = current.next;
            current.next = previous;
            previous = current;
            current = next;
        }
        first = previous;
    }

    public E get(int index) {
        // начинаем с первого элемента
        Node<E> current = first;

        int i = 0;

        while (i < size) {
            // если нашли нужный индекс
            if (i == index) {
                // возвращаем значение текущего узла
                return current.value;
            }
            // иначе идем дальше
            current = current.next;
            i++;
        }

        return null;
    }

    @Override
    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }
}
