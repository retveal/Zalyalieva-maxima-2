public class ArrayList<E> implements List<E> {

    private final static int DEFAULT_SIZE = 10;

    // хранилище элементов
    private E[] elements;
    // количество фактических элементов в списке
    private int size;

    public ArrayList() {
        // при создании списка я создал внутри массив на 10 элементов
        this.elements = (E[]) new Object[DEFAULT_SIZE];
    }

    /**
     * Добавление элемента в конец списка
     *
     * @param element добавляемый элемент
     */
    public void add(E element) {
        // если у меня переполнен массив
        ensureSize();

        this.elements[size++] = element;
    }

    public E get(int index) {
        return this.elements[index];
    }

    public int size() {
        return size;
    }

    private void ensureSize() {
        if (isFullArray()) {
            resize();
        }
    }
    // удаляет все элементы из списка
    public void clear() {
        size = 0;
    }

    public boolean equals(Object object) {
        return (this == object);
    }
    // удаляет элемент в заданном идексе, смещая элементы, которые идут после удаляемого на 1 позицию влево
    public void removeAt (int index) {
        // если такого индекса нет
        if (!isCorrectIndex(index)) {
            return;
        }
        for (int nextIndex = index + 1; nextIndex < size; nextIndex++) {
            this.elements[nextIndex - 1] = elements[nextIndex];
        }
        size --;
    }
    /**
     * удаляет первое вхождение элемента в список
     * @param element
     */
    public void remove(E element) {
        for (int i = 0; i < elements.length; i++) {
            if (this.elements[i] == element) {
                removeAt(i);
                return;
            }
        }
    }
    /**
     * удаляет последнее вхождение элемента в список
     * @param element
     */
    public void removeLast(E element) {
        // в обратном порядке
        for (int i = size - 1; i < elements.length; i--) {
            if (this.elements[i] == element) {
                removeAt(i);
                return;
            }
        }
    }
    /**
     * удаляет все вхождения элемента в список
     * @param element
     */
    public void removeALl(E element) {
        for (int i = 0; i < elements.length; i++) {
            if (this.elements[i] == element) {
                removeAt(i);
            }
        }
    }
    /**
     * вставляет элемент в заданный индекс (проверяет условие index < size)
     * @param index куда вставляем элемент
     * @param element элемент, который будем вставлять
     */
    public void add(int index, E element) {
        if (!isCorrectIndex(index)) {
            return;
        }
        for (index = index; index < size; index++) {
            E temp = get(index);
            elements[index] = element;
            element = temp;
        }
    }
    private boolean isCorrectIndex(int index) {
        return index >= 0 && index <= size;
    }

    private void resize() {
        // создаем новый массив, который в полтора раза больше, чем предыдущий
        E[] newArray = (E[]) new Object[this.elements.length + elements.length / 2];
        // копируем элементы из старого массива в новый поэлементно
        for (int i = 0; i < size; i++) {
            newArray[i] = this.elements[i];
        }
        // заменяем ссылку на старый массив ссылкой на новый массив
        // старый массив будет удален java-машиной
        this.elements = newArray;
    }

    private boolean isFullArray() {
        return size == elements.length;
    }
}