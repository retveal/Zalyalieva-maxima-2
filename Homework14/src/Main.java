public class Main {

    // Дореализовать обобщенный ArrayList, LinkedList

    public static void printList(List<String> strings) {
        for (int i = 0; i < strings.size(); i++) {
            System.out.println(strings.get(i));
        }
    }

    public static void printSumOfElements(List<Integer> integers) {
        int sum = 0;
        for (int i = 0; i < integers.size(); i++) {
            sum += integers.get(i);
        }
        System.out.println(sum);
    }

    public static void main(String[] args) {
        List<String> stringsByArrayList = new ArrayList<>();
        stringsByArrayList.add("Hello");
        stringsByArrayList.add("Word");
        stringsByArrayList.add("Bye");

        List<String> stringsLinkedList = new LinkedList<>();
        stringsLinkedList.add("Привет");
        stringsLinkedList.add("Слово");
        stringsLinkedList.add("Пока");

        List<Integer> integersByArrayList = new ArrayList<>();
        integersByArrayList.add(100);
        integersByArrayList.add(200);
        integersByArrayList.add(300);

        List<Integer> integersByLinkedList = new LinkedList<>();
        integersByLinkedList.add(400);
        integersByLinkedList.add(500);
        integersByLinkedList.add(600);

        printList(stringsLinkedList);
        printList(stringsByArrayList);
        printSumOfElements(integersByArrayList);
        printSumOfElements(integersByLinkedList);

    }
}