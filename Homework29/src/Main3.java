import validators.ProductNameValidator;
import validators.ProductNameRegexValidator;

public class Main3 {
    public static void main(String[] args) {
        ProductNameValidator regexValidator = new ProductNameRegexValidator("^[a-zA-Z\\s]+");

        regexValidator.validate("Juice");
    }
}
