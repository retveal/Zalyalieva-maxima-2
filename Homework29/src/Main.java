import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import repositories.ProductRepositoryNamedParameterJdbcTemplateImpl;
import repositories.ProductsRepository;
import services.ProductsService;
import services.ProductsServiceImpl;
import validators.ProductNameFormatValidator;
import validators.ProductNameValidator;
import validators.SupplierNameFormatValidator;
import validators.SupplierNameValidator;

public class Main {

    public static void main(String[] args){
        HikariConfig config = new HikariConfig();
        config.setUsername("postgres");
        config.setPassword("qwerty007");
        config.setJdbcUrl("jdbc:postgresql://localhost:5433/maxima_2");
        config.setDriverClassName("org.postgresql.Driver");
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);

        ProductsRepository productsRepository = new ProductRepositoryNamedParameterJdbcTemplateImpl(dataSource);

        ProductNameValidator productNameValidator = new ProductNameFormatValidator();
        SupplierNameValidator supplierNameValidator = new SupplierNameFormatValidator();

        ProductsService productsService = new ProductsServiceImpl(productsRepository, productNameValidator, supplierNameValidator);


    }
}
