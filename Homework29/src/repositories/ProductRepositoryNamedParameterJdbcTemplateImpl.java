package repositories;

import models.Product;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.util.*;

public class ProductRepositoryNamedParameterJdbcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_INSERT_INTO_FOOD_STUFFS = "insert into food_stuffs(product_name, price, supplier) " +
            "values (:product_name, :price, :supplier)";

    //language=SQL
    private static final String SQL_SELECT_BY_PRODUCT_NAME = "select * from food_stuffs" +
            " where product_name = :product_name limit 1";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_PRICE = "select * from food_stuffs" +
            " where price = :price";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from food_stuffs " +
            "where id = :id";

    //language=SQL
    private static final String SQL_UPDATE_PRODUCT_BY_ID = "update food_stuffs set product_name = :product_name, price = :price, expiration_date = :expiration_date, supplier = :supplier " +
            "where id = :id";

    //language=SQL
    private static final String SQL_DELETE_PRODUCT_BY_ID = "delete from food_stuffs where id = ?";

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public ProductRepositoryNamedParameterJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productsRowMapper = (row, rowNumber) -> Product.builder()
            .id(row.getLong("id"))
            .product_name(row.getString("product_name"))
            .price(row.getDouble("price"))
            .expiration_date(row.getString("expiration_date"))
            .supplier(row.getString("supplier"))
            .build();

    @Override
    public void save(Product product) {
        // объект, который будет сохранять сгенерированные ключи для данного запроса
        KeyHolder keyHolder = new GeneratedKeyHolder();

        Map<String, Object> params = new HashMap<>();

        params.put("product_name", product.getProduct_name());
        params.put("price", product.getPrice());
        params.put("supplier", product.getSupplier());

        namedParameterJdbcTemplate.update(SQL_INSERT_INTO_FOOD_STUFFS, new MapSqlParameterSource(params), keyHolder, new String[]{"id"});

        Long generatedId = ((Integer) keyHolder.getKeys().get("id")).longValue();

        product.setId(generatedId);

    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_BY_PRICE,
                Collections.singletonMap("price", price),
                productsRowMapper);
    }

    @Override
    public Optional <Product> findById(Long id) {
        try {
            return Optional.of(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id),
                    productsRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Product> findOneByName(String product_name) {
        try {
            return Optional.of(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_PRODUCT_NAME,
                    Collections.singletonMap("product_name", product_name),
                    productsRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void update(Product product) {
        Map<String, Object> params = new HashMap<>();

        params.put("id", product.getId());
        params.put("product_name", product.getProduct_name());
        params.put("price", product.getPrice());
        params.put("supplier", product.getSupplier());

        namedParameterJdbcTemplate.update(SQL_UPDATE_PRODUCT_BY_ID, params);

    }

    @Override
    public void delete(Product product) {
        Map<String, Object> params = new HashMap<>();

        params.put("id", product.getId());
        params.put("product_name", product.getProduct_name());
        params.put("price", product.getPrice());
        params.put("supplier", product.getSupplier());

        namedParameterJdbcTemplate.update(SQL_DELETE_PRODUCT_BY_ID, params);
    }
}
