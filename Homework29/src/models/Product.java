package models;

import lombok.*;

@Setter
@Getter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode

public class Product {

    private Long id;
    private String product_name;
    private double price;
    private String expiration_date;
    private String supplier;
}
