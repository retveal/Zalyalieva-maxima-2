package validators;

import validators.exceptions.SupplierNameValidatorException;

public class SupplierNameFormatValidator implements SupplierNameValidator{

    @Override
    public void validate(String supplier) throws SupplierNameValidatorException {
        if (supplier.length() < 3) {
            throw new SupplierNameValidatorException();
        }
    }
}
