package validators;

import validators.ProductNameValidator;
import validators.exceptions.ProductNameValidatorException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductNameRegexValidator implements ProductNameValidator {

    private Pattern regexPattern;

    public ProductNameRegexValidator(String regex) {
        this.regexPattern = Pattern.compile(regex);
    }

    @Override
    public void validate(String product_name) throws ProductNameValidatorException {
        Matcher matcher = regexPattern.matcher(product_name);
        if (!matcher.matches()) {
            throw new ProductNameValidatorException();
        }

    }
}
