package validators;

import validators.exceptions.ProductNameValidatorException;

public class ProductNameFormatValidator implements ProductNameValidator {
    @Override
    public void validate(String product_name) throws ProductNameValidatorException {
        if (product_name.length() <= 3) {
            throw new ProductNameValidatorException();
        }
    }
}
