public interface Set<E> {
    void add(E element);
    boolean contains(E element);
}
