// Используя Set и Map, которые мы реализовали
//для входной строки посчитать, сколько раз встречается каждое слово из входного текста
//
//ВХОД:
//привет привет марсель в казани ты тоже в казани
//
//ОТВЕТ:
//Привет - 2 раза
//марель - 1 раз
//ты - 1 раз
//тоже - 1 раз
//в - 2 раза
//казани - 2 раза
//
//String text = scanner.nextLine();
//String words[] = text.split(" "); // Разбить текст на слова можно

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, Integer> texts = new MapHashImpl<>();
        int count;

        String text = scanner.nextLine();
        String words[] = text.split(" ");

        for (int i = 0; i < words.length; i++) {
            // проверяем, есть ли уже это слово
            if (texts.containsKey(words[i])) {
                count = texts.get(words[i]);
                count++;
                texts.put(words[i], count);
            } else {
                // если слово не повторяется
                texts.put(words[i], 1);
            }
        }

        System.out.println("Слово привет в данном предложении повторяется " + texts.get("привет") + " раз(а)");
        System.out.println("Слово марсель в данном предложении повторяется " + texts.get("марсель") + " раз(а)");
        System.out.println("Слово ты в данном предложении повторяется " + texts.get("ты") + " раз(а)");
        System.out.println("Слово тоже в данном предложении повторяется " + texts.get("тоже") + " раз(а)");
        System.out.println("Слово в в данном предложении повторяется " + texts.get("в") + " раз(а)");
        System.out.println("Слово казани в данном предложении повторяется " + texts.get("казани") + " раз(а)");
    }
}
