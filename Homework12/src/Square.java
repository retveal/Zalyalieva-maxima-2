public class Square extends Rectangle {

    public Square(Center center, double a) {
        super(center, a, a);
    }

    public void printPerimeter() {
        System.out.println("Периметр квадрата равен " + getPerimeter());
        getPerimeter();
    }
}