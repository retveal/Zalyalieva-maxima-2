public class Circle extends Ellipse {

    public Circle(Center center, double r1) {
        // вызов конструктора предка
        super(center, r1, r1);
    }

    public void printPerimeter() {
        System.out.println("Периметр круга равен " + getPerimeter());
        getPerimeter();
    }
}
