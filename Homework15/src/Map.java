public interface Map<S, V> {

    void put(S string, V value);
}
