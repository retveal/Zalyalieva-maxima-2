// реализовать в UsersRepository findAll() и findByFirstName()
public class Main {

    public static void main(String[] args) {
        UsersRepositoryFileBasedImpl usersRepository = new UsersRepositoryFileBasedImpl("users.txt");
        User marsel = new User("Марсель", "Сидиков");
        User maxim = new User("Максим", "Анисимов");
        User ravil = new User("Равиль", "Фахрутдинов");
        User aisylu = new User("Айсылу", "Залялиева");
        User marta = new User("Марта", "Лазарева");
        User valeria = new User("Валерия", "Степанова");
        User damir = new User("Дамир", "Маркелов");

//        usersRepository.save(marsel);
//        usersRepository.save(maxim);
//        usersRepository.save(ravil);
//        usersRepository.save(aisylu);
//        usersRepository.save(marta);
//        usersRepository.save(valeria);
//        usersRepository.save(damir);

        System.out.println(usersRepository.findAll());
        System.out.println(usersRepository.findByFirstName("Айсылу"));
        System.out.println(usersRepository.findByFirstName("Марта"));
        System.out.println(usersRepository.findByFirstName("Дамир"));
        System.out.println(usersRepository.findByFirstName("Артур"));
    }
}
