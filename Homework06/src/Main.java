import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int[] array = {20, 40, 79, -15, 35, 67, 19, 186};
        Scanner scanner = new Scanner(System.in);
        int element = scanner.nextInt();

        selectionSort(array);
        System.out.println(search(array, element));
    }

    public static void selectionSort(int[] array) {
        int min, indexOfMin;

        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexOfMin = i;

            for (int j = i; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    indexOfMin = j;
                }
            }
            int temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;
        }
    }
    public static boolean search (int[] array, int element) {
        boolean hasElement = false;

        int left = 0;
        int right = array.length - 1;
        int middle = left + (right - left) / 2;

        while (left <= right) {
            if (element < array[middle]) {
                right = middle - 1;
            } else if (element > array[middle]) {
                left = middle + 1;
            } else {
                hasElement = true;
                break;
            }
            middle = left + (right - left) / 2;
        }
        return hasElement;
    }
}
