import java.util.Scanner;

class program2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int min = num;
        int localMin = 0;

        while (num != -1) {
            num = scanner.nextInt();
            if (num < min && num != -1) {
                min = num;
                num = scanner.nextInt();
                if (num > min && num != -1) {
                    localMin++;
                }
            }
        }
        System.out.println(localMin);
    }
}
