import java.util.Scanner;

class program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int digitsSum = 0;

        while (num != 0) {
            int lastDigit = num % 10;
            num /= 10;
            digitsSum += lastDigit;
        }
        num = scanner.nextInt();
        int min = num;
        while (num != -1) {
            if (num < min) {
                min = num;
            }
            num = scanner.nextInt();
        }
        System.out.println(min);
    }
}
