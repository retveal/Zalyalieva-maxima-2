// считать из файла текст, выяснить, какие слова считаются чаще других, и вывести их на экран
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) {

        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream("input.txt");
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        // используя массив символов вернем кол-во байтов
        char[] characters;
        try {
            characters = new char[inputStream.available()];
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        int position = 0;

        try {
            // считываем текущий байт
            int currentByte = inputStream.read();

            while (currentByte != -1) {
                // преобразуем байт в символ
                char character = (char) currentByte;
                // кидаем символ в массив
                characters[position] = character;
                position++;
                // считываем байт заново
                currentByte = inputStream.read();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        // создаем строку на основе массива
        String text = new String(characters);

        // создаем массив words, преобразуя все прописные символы в строчные и заменяя все знаки препинания
        String[] words = text.toLowerCase().replaceAll("[-.?!)(,:]", "").split("\\s");
//        System.out.println(Arrays.toString(words));

        Map<String, Integer> counts = new HashMap<>();
        // пробегаемся по массиву всех слов
        for (String word : words) {
            if(!word.isEmpty()) {
                Integer count = counts.get(word);
                // если слово попадается в тексте первый раз
                if (count == null) {
                    // присваиваем переменной count значение 0
                    count = 0;
                }
                count++;
                int result = count;
                counts.put(word, result);
            }
        }
        
        for(String word : counts.keySet()) {
            System.out.println(counts.get(word) + " - " + word);
        }
    }
}
