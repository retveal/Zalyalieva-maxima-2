// список на основе массива
public class ArrayList {

    private final static int DEFAULT_SIZE = 10;

    // хранилище элементов
    private int elements[];
    // кол-во фактических элементов в списке
    private int size;

    public ArrayList() {
        // при создании списка мы создали внутри массив на 10 элементов
        this.elements = new int[DEFAULT_SIZE];
    }

    /**
     * добавление элемента в конец списка
     * @param element  добавляемый элемент
     */
    public void add(int element) {
        // если массив переполнен
        if (isFullArray()) {
            resize();
        }
        this.elements[size++] = element;
    }

    private void resize() {
        // создаем новый массив, который в полтора раза больше, чем предыдущий
        int[] newArray = new int[this.elements.length + elements.length / 2];
        // копируем элементы из старого массива в новый поэлементно
        for (int i = 0; i < size(); i++) {
            newArray[i] = this.elements[i];
        }
        // заменяем ссылку на старый массив ссылкой на новый
        // старый элемент будет удален java-машиной
        this.elements = newArray;
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    /**
     * получение элемента по индексу
     * @param index индекс элемента
     * @return элемент, который был добавлен в список под номером index, если такого индекса нет - ошибка (у нас -1)
     */
    public int get(int index) {
        if (isCorrectIndex(index)) {
            return elements[index];
        } else {
            System.err.println("В списке нет такого индекса");
            return -1;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    /**
     * удаляет все элементы из списка
     */
    public void clear() {
        size = 0;
    }

    /**
     * возвращает кол-во элементов из списка
     * @return размер списка
     */
    public int size() {
        return -1;
    }

    /**
     * удаляет элемент в заданном индексе, смещая элементы, которые идут после удаляемого на 1 позицию влево
     * @param index индекс удаляемого элемента
     */
    public void removeAt(int index) {
        // если такого индекса нет
        if (!isCorrectIndex(index)) {
            return;
        }
        for (int nextIndex = index + 1; nextIndex < size; nextIndex++) {
            this.elements[nextIndex - 1] = elements[nextIndex];
        }
        size --;
    }

    /**
     * удаляет первое вхождение элемента в список
     * @param element
     */
    public void remove(int element) {
        for (int i = 0; i < elements.length; i++) {
            if (this.elements[i] == element) {
                removeAt(i);
                return;
            }
        }
    }

    /**
     * удаляет последнее вхождение элемента в список
     * @param element
     */
    public void removeLast(int element) {
        // в обратном порядке
        for (int i = size - 1; i < elements.length; i--) {
            if (this.elements[i] == element) {
                removeAt(i);
                return;
            }
        }
    }

    /**
     * удаляет все вхождения элемента в список
     * @param element
     */
    public void removeALl(int element) {
        for (int i = 0; i < elements.length; i++) {
            if (this.elements[i] == element) {
                removeAt(i);
            }
        }
    }

    /**
     * вставляет элемент в заданный индекс (проверяет условие index < size)
     * @param index куда вставляем элемент
     * @param element элемент, который будем вставлять
     */
    public void add(int index, int element) {
        if (!isCorrectIndex(index)) {
            return;
        }
        for (index = index; index < size; index++) {
            int temp = get(index);
            elements[index] = element;
            element = temp;
        }
    }
}
