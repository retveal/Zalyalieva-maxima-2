public class LinkedList {

    private static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }
    // ссылка на первый элемент
    private Node first;
    // ссылка на последний элемент
    private Node last;

    private int size = 0;

    public void add(int element) {
        // для нового элемента создаем узел
        Node newNode = new Node(element);
        // если список пустой
        if (isEmpty()) {
            // первый узел = новый узел
            first = newNode;
        } else {
            // следующий после последнего - это новый узел
            last.next = newNode;
        }
        // теперь новый узел - последний
        last = newNode;

        size++;
    }

    public void addToBegin(int element) {
        // для нового элемента создаем узел
        Node newNode = new Node(element);

        if (isEmpty()) {
            last = newNode;
        } else {
            // для нового узла, следующий после него - это первый узел списка
            newNode.next = first;
        }

        // теперь новый узел - первый
        first = newNode;
        size++;
    }

    public int get(int index) {
        // начинаем с первого элемента
        Node current = first;

        int i = 0;

        while (i < size) {
            // если нашли нужный индекс
            if (i == index) {
                // возвращаем значение текущего узла
                return current.value;
            }
            // иначе идем дальше
            current = current.next;
            i++;
        }

        System.err.println("В списке нет такого индекса");
        return -1;
    }

    public void removeAt(int index) {
        Node previousRemove = first;

        if (index == 0) {
            // если удаляется первый элемент, просто сдвигаем ссылку на него
            first = first.next;
            size--;
            return;
        }

        // доходим до индекса элемента, который стоит перед удаляемым
        for (int i = 0; i < index - 1; i++) {
            // переходим по узлам
            previousRemove = previousRemove.next;
        }
        // если удаляем последний
        if (index == (size - 1)) {
            last = previousRemove;
        }
        // в current находится узел, который стоит перед удаляемым
        Node forRemove = previousRemove.next;
        previousRemove.next = forRemove.next;

        size--;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void remove(int element) {
        Node current = first;

        // если удаляемый элемент находится в самом начале списка
        if (element == current.value) {
            first = first.next;

            size--;
            return;
        }
        // если удаляемый элемент находится в середине списка или в самом конце
        while (current.next != null) {
            if (current.next.value == element) {
                current.next = current.next.next;

                size--;
                return;
            }
            current = current.next;
        }
    }

    public void removeLast(int element) {
        Node current = first;

        while (current.next != null) {
            // если текущий элемент не равен удаляемому элементу
            if (current.value != element) {
                // то переходим к следующему
                current = current.next;
            } else {
                while (current != null) {
                    current = current.next;
                    if (current.next.value != element) {
                    }
                    else {
                        // удаляем крайний элемент
                        Node forRemove = current.next;
                        current.next = forRemove.next;
                        return;
                    }
                    size --;
                }
            }
        }
    }

    public void removeAll(int element) {
        Node current = first;
        // если удаляемый элемент первый в списке
        if (element == current.value) {
            first = first.next;

            size--;
        }
        // если удаляемый элемент находится в середине списка или в самом конце
        while (current.next != null) {
            if (current.next.value == element) {
                while (current.next.value == element) {

                    current.next = current.next.next;
                }

                size--;
            }
            current = current.next;
        }
    }

    public void add(int index, int element) {
        Node current = first;
        Node newElement = new Node(element);
        // новый узел
        Node newNode = new Node(index);


        // если индекс добавляемого элемента равен 0
        if (index == 0) {
            // следующий элемент после нового - предыдущий первый
            newElement.next = first;
            // добавляемый элемент - первый
            first = newElement;

            size++;
            return;
        }

        current = current.next;

        // если добавляемый элемент последний
        if (index == size - 1) {
            last.next = newElement;

            // теперь новый узел - последний
            last = newElement;

            size++;
            return;
        }

        // если добавляемый элемент в середине или в конце списка
        for (int i = 1; i < size; i++) {
            // доходим до элемента, который должен стоять перед добавляемым
            if (i == index - 1) {
                // следующий элемент после добавляемого - следующий элемент списка
                newElement.next = current.next;
                // следующий элемент - добавляемый
                current.next = newElement;

                size++;
                return;
            }
            current = current.next;
        }
    }

    public void reverse() {
        // если список пустой
        if (isEmpty()) {
            return;
        }

        Node previous = null;
        Node current = first;

        while (current != null) {
            Node next = current.next;
            current.next = previous;
            previous = current;
            current = next;
        }
        first = previous;
    }
}
