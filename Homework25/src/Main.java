import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Main {
    // language=SQL
    private static final String SQL_SELECT_FROM_FOOD = "select * from food_stuffs order by id";


    public static void main(String[] args) {
        Properties properties;

        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        try (Connection connection = DriverManager.getConnection(
                properties.getProperty("db.url"),
                properties.getProperty("db.user"),
                properties.getProperty("db.password"));
             Statement statement = connection.createStatement()) {

            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_FROM_FOOD)){
                while (resultSet.next()) {
                    String productName = resultSet.getString("product_name");
                    String price = resultSet.getString("price");
                    String expirationDate = resultSet.getString("expiration_date");
                    String supplier = resultSet.getString("supplier");

                    System.out.println(productName + " " + price + " " + expirationDate + " " + supplier);
                }
            }
        } catch(SQLException e){
            throw new IllegalArgumentException(e);
        }
    }
}