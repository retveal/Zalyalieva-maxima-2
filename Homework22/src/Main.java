import models.Car;
import repositories.CarRepository;
import repositories.CarRepositoryImpl;

import java.util.List;
import java.util.stream.Collectors;

// Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
// Вывести цвет автомобиля с минимальной стоимостью. // min + map
// Среднюю стоимость Camry

public class Main {

    public static void main(String[] args) {
        CarRepository carRepository = new CarRepositoryImpl("input.txt");
        System.out.println(carRepository.findAll());

        List<Car> byColorOrMileage = carRepository.findByColorOrMileage("Black", 0);
        byColorOrMileage.stream().map(Car::getNumber).forEach(System.out::println);

        List<Car> uniqueModels = carRepository.numberOfUniqueModels(700000, 800000);
        System.out.println(uniqueModels.stream().map(Car::getModel).distinct().count());

        carRepository.colorWithMinPrice();

        carRepository.averagePrice();
    }
}
