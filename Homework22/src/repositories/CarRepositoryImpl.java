package repositories;

import models.Car;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarRepositoryImpl implements CarRepository {

    private String fileName;

    public CarRepositoryImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Car> toCarMapper = string -> {
        String[] parsedLine = string.replaceAll(" ", "").split("\\|");
        return new Car(parsedLine[0], parsedLine[1], parsedLine[2],
                Integer.parseInt(parsedLine[3]), Double.parseDouble(parsedLine[4]));
    };

    @Override
    public List<Car> findAll() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(toCarMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Car> findByColorOrMileage(String color, Integer mileage) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(toCarMapper)
                    .filter(car -> car.getColor().equals(color) || car.getMileage().equals(mileage)).collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    // количество уникальных моделей в ценовом диапазон
    @Override
    public List<Car> numberOfUniqueModels(Integer from, Integer to) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(toCarMapper)
                    .filter(car -> car.getPrice() >= from && car.getPrice() <= to).collect(Collectors.toList());
        } catch(IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    // цвет автомобиля с минимальной стоимостью
    @Override
    public void colorWithMinPrice() {
         try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
             System.out.println(reader.lines().map(toCarMapper)
                     .min(Comparator.comparing(Car::getPrice)).get().getColor());
         } catch (IOException e) {
             throw new IllegalArgumentException(e);
         }
    }

    // средняя стоимость Camry
    @Override
    public void averagePrice() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            System.out.println(reader.lines().map(toCarMapper)
                    .mapToDouble(Car::getPrice).average());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
