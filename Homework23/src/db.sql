drop table if exists food_stuffs;

create table food_stuffs (
                             id serial primary key,
                             product_name char(20),
                             price int,
                             expiration_date date,
                             supplier char(20)
);

insert into food_stuffs (product_name, price, expiration_date, supplier) values ('Milk', 50, '2022-02-04', 'MilkMan');
insert into food_stuffs (product_name, price, expiration_date, supplier) values ('Cream', 60, '2022-02-01', 'MilkMan');
insert into food_stuffs (product_name, price, expiration_date, supplier) values ('SourCream', 30, '2022-02-01', 'MilkMan');
insert into food_stuffs (product_name, price, expiration_date, supplier) values ('Rice', 100, '2022-01-15', 'CerealDelights');
insert into food_stuffs (product_name, price, expiration_date, supplier) values ('OatMeal', 90, '2022-01-02', 'CerealDelights');
insert into food_stuffs (product_name, price, expiration_date, supplier) values ('Tea', 250, '2021-10-07', 'TeaGuru');
insert into food_stuffs (product_name, price, expiration_date, supplier) values ('Coffee', 420, '2021-05-17', 'TeaGuru');

-- получить товары, которые дороже 100 рублей
select * from food_stuffs where price >= 100;

-- получить товары, которые были поставлены ранее 02-02-2020 года
select * from food_stuffs where expiration_date <= '2022-02-02';

-- получить названия всех поставщиков
select supplier from food_stuffs;