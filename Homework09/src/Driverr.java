public class Driverr {
    private String name;
    private int experience;
    private Bus bus;

    public Driverr(String name, int experience) {
        this.name = name;
        if (experience > 0) {
            this.experience = experience;
        } else {
            System.err.println("ОПЫТА У " + name + " НЕТ!");
        }
    }
    public void setBus(Bus bus) {
        this.bus = bus;
    }

    // начало движения автобуса
    public void drive(Bus bus) {
        // если автобус заполнен, то он едет
        if (bus.isFull()) {
            System.out.println("Автобус едет");
        } else {
            System.err.println("Автобус ожидает пассажиров");
        }
    }
    public void leftBus(Bus bus) {
        if (!bus.isGoing()) {
            if (this.bus != null) {
                this.bus.driverIsLeaving();

                System.out.println("Водитель " + this.name + " завершает свою рабочую смену");
            }
        } else {
            System.err.println("Водитель не может покинуть автобус во время движения!");
        }
    }
    public void change(Bus bus) {
        if (!bus.drIsLeaving()) {
        } else {
            System.out.println("Вместо него к работе приступает " + this.name);
        }
    }
}
