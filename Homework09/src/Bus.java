public class Bus {
    private int number;
    private String model;
    private Driverr driver;

    // массив пассажиров
    private Passenger[] passengers;
    // фактическое кол-во пассажиров на данный момент
    private int count;

    public Bus(int number, String model, int placesCount) {
        if (number > 0) {
            this.number = number;
        } else {
            this.number = 1;
        }
        this.model = model;
        // создали placesCount объектных переменных, в которых можно посадить пассажира
        this.passengers = new Passenger[placesCount];
    }
    public void incomePassenger(Passenger passenger) {
        // проверяем, не превысилили ли мы кол-во мест
        if (this.count < this.passengers.length) {
            this.passengers[count] = passenger;
            this.count++;
        } else {
            System.err.println("Автобус переполнен!");
        }
    }

    public void setDriver(Driverr driver) {
        this.driver = driver;
        driver.setBus(this);
    }

    public boolean isFull() {
        return this.count == passengers.length;
    }

    public boolean isGoing() {
        return this.count == passengers.length;
    }

    public void driverIsLeaving() {
        if (this.driver == driver) {
            this.driver = null;
        }
    }
    public void passIsLeaving(Passenger passenger) {
        if (this.passengers == passengers) {
            this.passengers = null;
        }
    }
    public boolean drIsLeaving() {
        return this.driver == null;
    }
}
