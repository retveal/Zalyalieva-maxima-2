public class Passenger {
    private String name;

    // объектная переменная - это поле, которое ссылается на какой-либо автобус
    private Bus bus;
    private Driverr driver;

    // перегруженные конструкторы
    public Passenger() {
        this.name = "Без имени";
    }

    public Passenger(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void printThis() {
        System.err.println(this);
    }
    // идем в автобус
    public void goToBus(Bus bus) {
        // проверяем: не в автобусе ли мы уже?
        if (this.bus != null) {
            System.err.println(name + ": Я уже в автобусе!");
        } else {
            if (!bus.isFull() && !bus.isGoing()) {
                // если автобуса еще не было
                this.bus = bus;
                // передаем в автобус себя
                this.bus.incomePassenger(this);
            } else {
                System.err.println(name + ": Я не попал в автобус :(");
            }
        }
    }

    public void pronouncedTheName(Bus bus) {
        if (bus.isGoing()) {
            System.out.println(name + ": Меня зовут " + name);
        }
    }

    // покидаем автобус
    public void leaveBus(Bus bus) {
        if (!bus.isGoing()) {
            if (this.bus != null) {
                this.bus.passIsLeaving(this);
                System.out.println("Пассажир " + name + " покинул автобус");
            }
        } else {
            System.err.println("Пассажиры не могут покинуть автобус во время движения!");
        }
    }
}
