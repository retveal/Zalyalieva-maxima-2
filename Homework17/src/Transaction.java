public class Transaction {

    private User from;
    private User to;
    private int sum;

    public Transaction(User from, User to, int sum) {
        this.from = from;
        this.to = to;
        this.sum = sum;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "from=" + from +
                ", to=" + to +
                ", sum=" + sum +
                '}';
    }

    public int getTransactionsSum() {
        return sum;
    }
}
