// Реализовать методы в Task2
// Использовать только встроенные коллекции - List, ArrayList, HashMap
//* Вывести все имена пользователей и количество их транзакций

public class Main {
    public static void main(String[] args) {
        // реализовать систему платежей
        // один пользователь может переводить деньги другому пользователю

        User marsel = new User("+79372824941", "Марсель");
        User rafael = new User("+79372824942", "Рафаэель");
        User aisylu = new User("+79179156981", "Айсылу");

        Bank bank = new Bank();
        bank.sendMoney(rafael, marsel, 100);
        bank.sendMoney(rafael, marsel, 300);
        bank.sendMoney(rafael, marsel, 200);
        bank.sendMoney(marsel, rafael, 50);
        bank.sendMoney(marsel, rafael, 100);
        bank.sendMoney(marsel, rafael, 120);

        bank.userAndTransactions();
    }
}
