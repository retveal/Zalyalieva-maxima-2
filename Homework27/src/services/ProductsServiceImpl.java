package services;


import models.Product;
import repositories.ProductsRepository;
import validators.ProductNameValidator;
import validators.SupplierNameValidator;

import javax.naming.AuthenticationException;
import java.util.Optional;


public class ProductsServiceImpl implements ProductsService {

    private final ProductsRepository productsRepository;

    private final ProductNameValidator productNameValidator;

    private final SupplierNameValidator supplierNameValidator;

    public ProductsServiceImpl(ProductsRepository productsRepository,
                               ProductNameValidator productNameValidator,
                               SupplierNameValidator supplierNameValidator) {
        this.productsRepository = productsRepository;
        this.productNameValidator = productNameValidator;
        this.supplierNameValidator = supplierNameValidator;
    }

    @Override
    public void signIn(String product_name, double price, String supplier) throws AuthenticationException {
        // находим product по его name
        Optional<Product> productOptional = productsRepository.findOneByName(product_name);
        // если мне пришел конкретный product
        if (productOptional.isPresent()) {
            // смотрим его supplier
            Product product = productOptional.get();
            if (!product.getSupplier().equals(supplier)) {
                throw new AuthenticationException();
            }
            // если все ок - просто останавливаем работу процедуры
            return;
        }
        throw new AuthenticationException();
    }

    @Override
    public void signUp(String product_name, Double price, String supplier) {
        productNameValidator.validate(product_name);
        supplierNameValidator.validate(supplier);

        Product product = Product.builder()
                        .product_name(product_name)
                        .price(price)
                        .supplier(supplier)
                        .build();
        productsRepository.save(product);
    }
}
