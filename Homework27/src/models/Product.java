package models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder

public class Product {

    private Long id;
    private String product_name;
    private double price;
    private String expiration_date;
    private String supplier;

}
