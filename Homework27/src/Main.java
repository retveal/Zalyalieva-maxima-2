import models.Product;
import repositories.ProductsRepository;
import util.factories.ProductsRepositoryFactory;

// Сделать приложение с одним ProductsRepository/ProductsRepositoryJdbcImpl
// Предусмотреть методы:
// List<Product> findAllByPrice(double price); // возвращает все товары одной стоимости
// Optional<Product> findOneByName(String name); // найти товар по названию
// void update(Product product); // обновить товар
// void delete(Product product); // удалить из базы данных

public class Main {

    public static void main(String[] args){
        ProductsRepository productsRepository = ProductsRepositoryFactory.onJdbc();

        Product product = productsRepository.findById(32L).orElseThrow(IllegalArgumentException::new);
        productsRepository.delete(product);

    }
}
