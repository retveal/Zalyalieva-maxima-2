public class Main {

    // функция, для которой считаем определенный интеграл
    public static double f(double x) {
        return Math.sin(x) * 2;
    }
    // метод Симпсона
    // функция, которая возвращает значение определенного интеграла для функции f(x) на промежутке от 0 до 10 с разбиением n
    public static double integralBySimpson(double a, double b, int n) {
        double width = (b - a) / n;
        double currentArea = 0;
        double sum = 0;

        for (double x = 0; x <= b; x += width * 2) {
            sum += width / 3 * (f(x - width)+ 4 * f(x) + f(x + width));
        }
        return sum;
    }

    public static void main(String[] args) {
        // массив с примерами количеств разбиений исходного диапазона
        int[] ns = {10, 100, 1000, 50_000, 100_000, 150_000, 200_000, 300_000, 500_000, 1_000_000, 5_000_000};
        double from = 0;
        double to = 10;
        double realValue = 3.67814305815;
        // массив с результатами работы функции integral для разных разбиений, в ys[i] находятся значения для разбиения ns[i]
        double[] ys = new double[ns.length];
        // считаем интегралы для всех разбиений на промежутке от 0 до 10
        for (int i = 0; i < ns.length; i++) {
            ys[i] = integralBySimpson(from, to, ns[i]);
        }
        // вывод результата
        for (int i = 0; i < ns.length; i++) {
            System.out.printf("| N = %10d | Y = % 10.4f | EPS = %10.5f | \n", ns[i], ys[i], Math.abs(ys[i] - realValue));
        }
    }
}
