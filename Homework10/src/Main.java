import java.util.Scanner;

public class Main {

    //Сделать класс Logger с методом void log(String message), который выводит в консоль какое-либо сообщение.
    //Применить паттерн Singleton для Logger.

    public static void main(String[] args) {
        Logger logger1 = Logger.getInstance();
        Logger logger2 = Logger.getInstance();

        logger1.log("Marsel");
        logger2.log("Help me with task 7 please)))");
    }
}
