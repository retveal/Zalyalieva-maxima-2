package ru.maxima.blog.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.blog.models.Product;

public interface ProductsRepository extends JpaRepository<Product, Long> {
    Page<Product> findAllByState(Product.State state, Pageable pageable);
}
