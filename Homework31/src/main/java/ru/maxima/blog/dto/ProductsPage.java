package ru.maxima.blog.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductsPage {
    List<ProductDto> products;
    private Integer pageSize;
    private Integer totalPages;
}
