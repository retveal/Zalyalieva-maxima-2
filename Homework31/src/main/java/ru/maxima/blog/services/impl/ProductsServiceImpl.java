package ru.maxima.blog.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.maxima.blog.dto.ProductDto;
import ru.maxima.blog.dto.ProductsPage;
import ru.maxima.blog.exceptions.ProductNotFoundException;
import ru.maxima.blog.models.Product;
import ru.maxima.blog.repositories.ProductsRepository;
import ru.maxima.blog.services.ProductsService;

import static ru.maxima.blog.dto.ProductDto.from;

@Service
@RequiredArgsConstructor
public class ProductsServiceImpl implements ProductsService {

    private final ProductsRepository productsRepository;

    private final static int DEFAULT_PAGE_SIZE = 5;

    @Override
    public ProductsPage getAll(int page) {
        PageRequest pageRequest = PageRequest.of(page, DEFAULT_PAGE_SIZE, Sort.by("id"));
        Page<Product> products = productsRepository.findAllByState(Product.State.REGISTERED, pageRequest);
        return ProductsPage.builder()
                .products(from(products.getContent()))
                .pageSize(products.getSize())
                .totalPages(products.getTotalPages())
                .build();
    }

    @Override
    public ProductDto addProduct(ProductDto product) {
        Product newProduct = Product.builder()
                .name(product.getName())
                .supplier(product.getSupplier())
                .state(Product.State.UNAVAILABLE)
                .build();

        productsRepository.save(newProduct);

        return from(newProduct);
    }

    @Override
    public ProductDto getProduct(Long productId) {
        return from(getProductOrThrow(productId));
    }

    private Product getProductOrThrow(Long productId) {
        return productsRepository.findById(productId).orElseThrow(ProductNotFoundException::new);
    }

    @Override
    public ProductDto updateProduct(Long productId, ProductDto newProductData) {
        Product product = getProductOrThrow(productId);

        product.setName(newProductData.getName());
        product.setSupplier(newProductData.getSupplier());

        productsRepository.save(product);

        return from(product);
    }

    @Override
    public void deleteProduct(Long productId) {
        Product product = getProductOrThrow(productId);

        product.setState(Product.State.MISSING);

        productsRepository.save(product);
    }

    @Override
    public void registerProduct(Long productId) {
        Product product = getProductOrThrow(productId);

        product.setState(Product.State.REGISTERED);

        productsRepository.save(product);
    }

    @Override
    public void unavailableProduct(Long productId) {
        Product product = getProductOrThrow(productId);

        product.setState(Product.State.UNAVAILABLE);

        productsRepository.save(product);
    }
}
