package ru.maxima.blog.services;

import ru.maxima.blog.dto.ProductDto;
import ru.maxima.blog.dto.ProductsPage;

public interface ProductsService {
    ProductsPage getAll(int page);

    ProductDto addProduct(ProductDto product);

    ProductDto getProduct(Long productId);

    ProductDto updateProduct(Long productId, ProductDto newProductData);

    void deleteProduct(Long productId);

    void registerProduct(Long productId);

    void unavailableProduct(Long productId);
}
