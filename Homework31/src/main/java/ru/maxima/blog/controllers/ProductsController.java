package ru.maxima.blog.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.maxima.blog.dto.ProductDto;
import ru.maxima.blog.dto.ProductsPage;
import ru.maxima.blog.services.ProductsService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductsController {

    private final ProductsService productsService;

    @GetMapping
    public ResponseEntity<ProductsPage> getAllProducts(@RequestParam("page") int page) {
        return ResponseEntity
                .ok(productsService.getAll(page));
    }

    @PostMapping
    public ResponseEntity<ProductDto> productsDto(@RequestBody ProductDto product) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(productsService.addProduct(product));
    }

    @GetMapping("/{product-id}")
    public ResponseEntity<ProductDto> getProduct(@PathVariable("product-id") Long productId) {
        return ResponseEntity
                .ok(productsService.getProduct(productId));
    }

    @PutMapping("/{product-id}")
    public ResponseEntity<ProductDto> updateProduct(@PathVariable("product-id") Long productId,
                                                 @RequestBody ProductDto newProductData) {
        return ResponseEntity
                .accepted()
                .body(productsService.updateProduct(productId, newProductData));
    }

    @DeleteMapping("/{product-id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("product-id") Long productId) {
        productsService.deleteProduct(productId);
        return ResponseEntity
                .accepted()
                .build();
    }

    @PostMapping(value = "/{product-id}", params = "action=registered")
    public ResponseEntity<?> registerProduct(@PathVariable("product-id") Long productId) {
        productsService.registerProduct(productId);
        return ResponseEntity
                .accepted()
                .build();
    }

    @PostMapping(value = "/{product-id}", params = "action=unavailable")
    public ResponseEntity<?> unavailableProduct(@PathVariable("product-id") Long productId) {
        productsService.unavailableProduct(productId);
        return ResponseEntity
                .accepted()
                .build();
    }
}
