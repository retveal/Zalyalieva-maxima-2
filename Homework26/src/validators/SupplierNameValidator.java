package validators;

import validators.exceptions.SupplierNameValidatorException;

public interface SupplierNameValidator {
    void validate(String supplier) throws SupplierNameValidatorException;
}
