package util.factories;

import repositories.ProductsRepository;
import repositories.ProductsRepositoryJdbcImpl;
import util.jdbc.DataSourceImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ProductsRepositoryFactory {

    public static ProductsRepository onJdbc() {
        Properties properties;

        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new DataSourceImpl(properties);
        return new ProductsRepositoryJdbcImpl(dataSource);
    }
}
