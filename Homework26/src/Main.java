import models.Product;
import repositories.ProductsRepository;
import util.factories.ProductsRepositoryFactory;

import javax.naming.AuthenticationException;

// Сделать приложение с одним ProductsRepository/ProductsRepositoryJdbcImpl
// Предусмотреть методы:
// List<Product> findAllByPrice(double price); // возвращает все товары одной стоимости
// Optional<Product> findOneByName(String name); // найти товар по названию
// void update(Product product); // обновить товар
// void delete(Product product); // удалить из базы данных

public class Main {

    public static void main(String[] args) throws AuthenticationException {
        ProductsRepository productsRepository = ProductsRepositoryFactory.onJdbc();

        // System.out.println(productsRepository.findAllByPrice(60));
        // System.out.println(productsRepository.findOneByName("Milk"));

        Product carrot = productsRepository.findById(13L).orElseThrow(IllegalArgumentException::new);
        System.out.println(carrot);
        // carrot.setExpiration_date("2022-03-24");
        // productsRepository.update(carrot);
        productsRepository.delete(carrot);



    }
}
