package services;

import javax.naming.AuthenticationException;

public interface ProductsService {

    /**
     * Проверяет, есть ли товар в базе данных с такими учетными данными
     * @param product_name
     * @param price
     * @param supplier
     * @throws AuthenticationException в случае, если данные неверные
     */
    void signIn (String product_name, double price, String supplier) throws AuthenticationException;

    /**
     * Регистрирует товар в системе
     * @param product_name
     */
    void signUp(String product_name, Double price, String expiration_date, String supplier);
}
