package services;


import models.Product;
import repositories.ProductsRepository;
import validators.ProductNameValidator;
import validators.SupplierNameValidator;

import javax.naming.AuthenticationException;
import java.util.Optional;


public class ProductsServiceImpl implements ProductsService {

    private final ProductsRepository productsRepository;

    private final ProductNameValidator productNameValidator;

    private final SupplierNameValidator supplierNameValidator;

    public ProductsServiceImpl(ProductsRepository productsRepository,
                               ProductNameValidator productNameValidator,
                               SupplierNameValidator supplierNameValidator) {
        this.productsRepository = productsRepository;
        this.productNameValidator = productNameValidator;
        this.supplierNameValidator = supplierNameValidator;
    }

    @Override
    public void signIn(String product_name, double price, String supplier) throws AuthenticationException {
        Optional<Product> productOptional = productsRepository.findOneByName(product_name);
        if (productOptional.isPresent()) {
            Product product = productOptional.get();
            if (!product.getProduct_name().equals(product_name)) {
                throw new AuthenticationException();
            }
            return;
        }
        throw new AuthenticationException();
    }

    @Override
    public void signUp(String product_name, Double price, String expiration_date, String supplier) {
        productNameValidator.validate(product_name);
        supplierNameValidator.validate(supplier);

        Product product = new Product(product_name, price, expiration_date, supplier);
        productsRepository.save(product);
    }
}
