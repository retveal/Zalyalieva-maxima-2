package models;

import java.util.Objects;
import java.util.StringJoiner;

public class Product {

    private Long id;
    private String product_name;
    private double price;
    private String expiration_date;
    private String supplier;

    public Product(String product_name, double price, String supplier) {
        this.product_name = product_name;
        this.price = price;
        this.supplier = supplier;
    }

    public Product(Long id, String product_name, double price, String expiration_date, String supplier) {
        this.id = id;
        this.product_name = product_name;
        this.price = price;
        this.expiration_date = expiration_date;
        this.supplier = supplier;
    }

    public Product(String product_name, double price, String expiration_date, String supplier) {
        this.product_name = product_name;
        this.price = price;
        this.expiration_date = expiration_date;
        this.supplier = supplier;
    }

    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(String expiration_date) {
        this.expiration_date = expiration_date;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Product.class.getSimpleName() + "[", "]")
                .add("id = " + id)
                .add("product_name = " + product_name)
                .add("price = " + price)
                .add("expiration_date = " + expiration_date)
                .add("supplier = " + supplier)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id)
                && Objects.equals(product_name, product.product_name)
                && Objects.equals(price, product.price)
                && Objects.equals(expiration_date, product.expiration_date)
                && Objects.equals(supplier, product.supplier);
    }
}
