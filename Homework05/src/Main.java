import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = 120;
        int[] array = new int[size];
        int age = 0;
        int hm = 0;

        System.out.println("Введите возраста от 0 до 120, завершая -1");

        int ages = scanner.nextInt();
        while (ages != -1) {
            array[ages]++;
            ages = scanner.nextInt();
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] > hm) {
                age = i;
                hm = array[i];
            }
        }
        System.out.println("Из всех перечисленных возрастов чаще всего встречается " + age + " лет - " + hm + " раз(а)");
    }
}
