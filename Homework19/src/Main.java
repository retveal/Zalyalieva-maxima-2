import java.io.FileInputStream;
import java.io.FileOutputStream;

// реализовать класс TextConverter, в котором будет метод toLowerCaseAll(String sourceFileName, String targetFileName)
// данный метод принимает на вход два имени файла
// в первом (исходном) файле дан текст на английском языке (и еще могут быть символы и все гарантированно не Unicode)
// метод считывает весь текст, все большие буквы делает маленькими и убирает все небуквенные символы, кроме пробела
// весь этот текст зависит в целевой файл с названием targetFileName
// можно использовать character.isLetter(); и character.toLowerCase();
public class Main {

    public static void main(String[] args) throws Exception {
        TextConverter.toLowerCaseAll("input.txt", "output.txt");
    }
}
